from Code.Create_customers import *
import pandas as pd

path = "./Data/"
inputfile = os.path.abspath("..\Data\Coffeebar_2013-2017.csv")

D_Frame = pd.read_csv(inputfile, sep=';')

# initialize
prob_arrive = {"regular_once": 72, "tripadvisor": 80, "regular_returning": 93, "hipsters": 100}
client_generate = {"regular_once": [], "tripadvisor": [], "regular_returning": [], "hipsters": []}

price_drink = {"milkshake": 5, "frappucino": 4, "water": 2, "coffee": 3, "soda": 3, "tea": 3}
price_food = {"sandwich": 5, "cookie": 2, "muffin": 2, "pie": 3, }

id_client = 1
nb_returnings = 1

# creation customer
while nb_returnings < 1001:
    prob_client = random.randint(1, 100)
    for key in prob_arrive:
        if (prob_client < prob_arrive[key]):
            type_client = key
            break

    if type_client == "regular_returning" or type_client == "hipsters":
        id = "id_" + str(id_client)
        id_client += 1
        client = "client_" + id
        if type_client == "regular_returning":
            client = returning_customer_regular(id)
        else:
            client = returning_customer_hipsters(id)

        nb_returnings += 1

        client_generate[type_client].append(client)

# tests

for info in client_generate:
    print(info)
    print(client_generate[info].__len__())

# dépenser
print("En cours de calcul...")
# init
add_proba = 0
incr = 2018
new_year = 1
deb = 1
consum = {}
id_client = 1001

for time in D_Frame['TIME']:
    print(deb)
    # generate proba
    ran = random.randint(1, 100)

    # creation a new date
    new_date = str(incr) + time[4:]
    new_year += 1
    deb += 1
    if ("12-31 17:56:00" in time):
        incr += 1
        new_year = 1

    # check which client comes to our shop
    for type_customer in prob_arrive:
        if (ran < prob_arrive[type_customer]):
            the_customer = type_customer
            break
    # clients' spend
    for i in client_generate:

        if (i == the_customer):
            # if it's a client that comes once
            if (i == "regular_once" or i == "tripadvisor"):
                id = "id_" + str(id_client)
                id_client += 1
                # client = "client_" + id
                if the_customer == "regular_once":
                    client = once_customer_regular(id)
                elif the_customer == "tripadvisor":
                    client = once_customer_tripadvisor(id)

                drunk = client.buy_drinks(new_date[11:16], D_Frame_PD)
                eaten = client.buy_food(new_date[11:16], D_Frame_PF)

                consum[(new_date, client.customerid)] = (drunk, eaten)

                client_generate[the_customer].append(client)

            # if it's a client that comes multiple times
            if (i == "regular_returning" or i == "hipsters"):
                for client in client_generate[i]:
                    if client.budget > 10:
                        drunk = client.buy_drinks(new_date[11:16], D_Frame_PD)
                        eaten = client.buy_food(new_date[11:16], D_Frame_PF)
                        price = 0

                        price = price_drink[drunk]
                        client.budget -= price

                        price = 0
                        if (eaten != "nothing"):
                            price = price_food[eaten]
                            client.budget -= price

                        consum[(new_date, client.customerid)] = (drunk, eaten)

                        break

print("calcul terminé")
print(client_generate)

print("########################################")
print("########################################")
print("########################################")
print("########################################")
print(consum)

#Doing some tests
print(client_generate['hipsters'][9].budget)
print(client_generate['regular_returning'][78].budget)
print(client_generate['regular_returning'][78].history)

#Generate files
list_new_data = []

for key2 in consum:
    list_new_data.append({'TIME': key2[0], 'CUSTOMER_ID': key2[1], 'DRINKS': consum[key2][0], 'FOOD': consum[key2][1]})
print("---------------")

DFinal = pd.DataFrame(list_new_data, columns=['TIME', 'CUSTOMER_ID', 'DRINKS', 'FOOD'])
DFinal.to_csv('..\Data\Coffebar_simulation.csv', encoding='utf-8', index=False, sep=';')
