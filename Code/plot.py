import pandas as pd
import os

inputfile = os.path.abspath("..\Data\Coffebar_simulation.csv")

DFinal = pd.read_csv(inputfile, sep=';')

#Make plots for amount of drinks and food
import matplotlib.pyplot as plt
values1 = DFinal['DRINKS'].value_counts().keys().tolist()
counts1 = DFinal['DRINKS'].value_counts().tolist()
fig, ax = plt.subplots()
ax.bar(values1, counts1)
ax.set_ylabel('Amount of sold drinks')
ax.set_xlabel('Type of drinks')
ax.set_title('Amount of sold drinks for each type')
plt.show()

import matplotlib.pyplot as plt
values1 = DFinal['FOOD'].value_counts().keys().tolist()
counts1 = DFinal['FOOD'].value_counts().tolist()
fig, ax = plt.subplots()
ax.bar(values1, counts1)
ax.set_ylabel('Amount of sold food')
ax.set_xlabel('Type of food')
ax.set_title('Amount of sold food for each type')
plt.show()