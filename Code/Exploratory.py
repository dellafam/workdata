import pandas as pd
import os
path = "./Data/"

print(os.path.abspath("."))
print(os.path.abspath(".."))
print(os.path.abspath("..\Data"))
inputfile = os.path.abspath("..\Data\Coffeebar_2013-2017.csv")

D_Frame = pd.read_csv(inputfile, sep=';')

print(D_Frame)

#What food and drinks are sold by the coffee bar?
print(D_Frame['DRINKS'].value_counts())

print(D_Frame['FOOD'].value_counts())

#How many unique customers did the bar have?
Once_customers = D_Frame['CUSTOMER']
count_1 = len([i for i in (Once_customers.value_counts()) if i == 1])
print(count_1)

Returning_customers = D_Frame['CUSTOMER']
count_2 = len([i for i in (Returning_customers.value_counts()) if i != 1])
print(count_2)

Unique_customers = count_1 + count_2
print(Unique_customers)

#Make plots for amount of drinks and food
import matplotlib.pyplot as plt
values1 = D_Frame['FOOD'].value_counts().keys().tolist()
counts1 = D_Frame['FOOD'].value_counts().tolist()
fig, ax = plt.subplots()
ax.bar(values1, counts1)
ax.set_ylabel('Amount of sold foods')
ax.set_xlabel('Type of foods')
ax.set_title('Amount of sold foods for each type')
plt.show()

import matplotlib.pyplot as plt
values2 = D_Frame['DRINKS'].value_counts().keys().tolist()
counts2 = D_Frame['DRINKS'].value_counts().tolist()
fig, ax = plt.subplots()
ax.bar(values2, counts2)
ax.set_ylabel('Amount of sold drinks')
ax.set_xlabel('Type of drinks')
ax.set_title('Amount of sold drinks for each type')
plt.show()

#percentages
print("\\\Percentages///")

#initialization of time
time_list=[]
i=0
while i<171:
    time_list.append(D_Frame['TIME'][i][11:16])
    i+=1

print(time_list)

# Create a list
list_data_food = []
list_data_drink = []

#finding the occurence for each drink (and each food) for each timestamp
for hour in time_list:
    value_elements_drinks = {"soda":0, "coffee":0, "water":0, "milkshake":0, "tea":0, "frappucino":0}
    value_elements_food = {"sandwich":0,"pie":0,"cookie":0,"muffin":0}
    print("---New iteration---")
    print(hour)

    info =0
    total_hour =0
    for ligne in D_Frame['TIME']:
        if ligne[11:16] == hour:
            total_hour +=1
            drunk = D_Frame['DRINKS'][info]
            for key1 in value_elements_drinks:
                if key1 == drunk:
                    value_elements_drinks[key1] += 1
            eaten = D_Frame['FOOD'][info]
            for key2 in value_elements_food:
                if key2 == eaten:
                    value_elements_food[key2] += 1

        info += 1
    print(value_elements_drinks)
    print(value_elements_food)
    print("Information for ", hour)
#Calculating the percentage from the occurence per drink per timestamp
    print("---Drink---")

    # create a dico:

    for key1 in value_elements_drinks:
        percentage_drinks = (value_elements_drinks[key1]/total_hour) *100
        list_data_drink.append({'TIME': hour, 'KIND': key1, 'PERCENTAGE' : percentage_drinks}) # CHANGER
        print("%s met in percent: %d " %(key1, percentage_drinks))
    print("---Food---")
    for key2 in value_elements_food:
        percentage_food = (value_elements_food[key2]/total_hour) *100
        list_data_food.append({'TIME': hour, 'KIND': key2, 'PERCENTAGE': percentage_food})
        print("%s met in percent: %d " %(key2, percentage_food))
    print("---------------")

DD = pd.DataFrame(list_data_drink, columns = ['TIME','KIND','PERCENTAGE'])
DD.to_csv('..\Data\Percentage_drinks.csv',encoding = 'utf-8', index = False,sep = ';')

DF = pd.DataFrame(list_data_food, columns = ['TIME','KIND','PERCENTAGE'])
DF.to_csv('..\Data\Percentage_food.csv',encoding = 'utf-8', index = False, sep = ';')