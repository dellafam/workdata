
import random
import os
import pandas as pd


inputfile = os.path.abspath("..\Data\Percentage_drinks.csv")
D_Frame_PD = pd.read_csv(inputfile, sep=';')

inputfile = os.path.abspath("..\Data\Percentage_food.csv")
D_Frame_PF = pd.read_csv(inputfile, sep=';')

class customer:

    def __init__(self, id):
        self.customerid = id
        self.paid = 0
        self.budget = 0

    def buy_food(self,time,file):
        prob_food = {"sandwich": 0,"pie": 0, "cookie": 0, "muffin": 0}
        prob = random.randint(1,99)
        where = 0
        cum_prob=0

        #get percent per hour for food
        for info in file["TIME"]:
            if info == time:
                for key in prob_food:
                    if file["KIND"][where] == key:
                        cum_prob+= file["PERCENTAGE"][where]
                        prob_food[key] = cum_prob
            where +=1
        #check which kind of food
        for key in prob_food:
            if (prob < prob_food[key]):
                kind = key
                break
            else:
                kind = "nothing"

        return kind

    def buy_drinks(self, time, file):
        prob_drinks = {"soda":0, "coffee":0, "water":0, "milkshake":0, "tea":0, "frappucino":0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0


        for info in file["TIME"]:
            if info == time:
                for key in prob_drinks:
                    if file["KIND"][where] == key:
                        prob_drinks[key] = file["PERCENTAGE"][where]
            where += 1

            # get percent per hour for drinks
            for info in file["TIME"]:
                if info == time:
                    for key in prob_drinks:
                        if file["KIND"][where] == key:
                            cum_prob += file["PERCENTAGE"][where]
                            prob_drinks[key] = cum_prob
                where += 1
            # check which kind of drinks
            for key in prob_drinks:
                if (prob <= prob_drinks[key]):
                    kind = key
                    break
                else:
                    kind = "nothing"

        return kind


"""once customer"""
class once_customer(customer):

    def __init__(self,*args,**kwargs):
        customer.__init__(self,*args,**kwargs)
        self.budget=100

    def buy_food(self, time, file):
        prob_food = {"sandwich": 0,"pie": 0, "cookie": 0, "muffin": 0}
        prob = random.randint(1,99)
        where = 0
        cum_prob = 0

        # get percent per hour for food
        for info in file["TIME"]:
            if info == time:
                for key in prob_food:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_food[key] = cum_prob
            where += 1
        # check which kind of food
        for key in prob_food:
            if (prob <= prob_food[key]):
                kind = key
                break
            else:
                kind = "nothing"
        return kind

    def buy_drinks(self, time, file):
        prob_drinks = {"soda": 0, "coffee": 0, "water": 0, "milkshake": 0, "tea": 0, "frappucino": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob =0
        # get percent per hour for drinks
        for info in file["TIME"]:
            if info == time:
                for key in prob_drinks:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_drinks[key] = cum_prob
            where += 1
        # check which kind of drinks
        for key in prob_drinks:
            if (prob <= prob_drinks[key]):
                kind = key
                break
        return kind


class once_customer_tripadvisor(once_customer):

    def __init__(self,*args,**kwargs):
        once_customer.__init__(self,*args,**kwargs)

    def buy_food(self, time, file):
        prob_food = {"sandwich": 0, "pie": 0, "cookie": 0, "muffin": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0

        # get percent per hour for food
        for info in file["TIME"]:
            if info == time:
                for key in prob_food:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_food[key] = cum_prob
            where += 1
        # check which kind of food
        for key in prob_food:
            if (prob <= prob_food[key]):
                kind = key
                break
            else:
                kind = "nothing"

        return kind

    def buy_drinks(self, time, file):
        prob_drinks = {"soda": 0, "coffee": 0, "water": 0, "milkshake": 0, "tea": 0, "frappucino": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob =0
        # get percent per hour for drinks
        for info in file["TIME"]:
            if info == time:
                for key in prob_drinks:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_drinks[key] = cum_prob
            where += 1
        # check which kind of drinks
        for key in prob_drinks:
            if (prob <= prob_drinks[key]):
                kind = key
                break

        return kind

class once_customer_regular(once_customer):

    def __init__(self,*args,**kwargs):
        once_customer.__init__(self,*args,**kwargs)



"""returning customer"""
class returning_customer(customer):

    def __init__(self,*args,**kwargs):
        customer.__init__(self,*args,**kwargs)
        self.history = {"soda": 0, "coffee": 0, "water": 0, "milkshake": 0, "tea": 0, "frappucino": 0, "sandwich": 0,
                        "pie": 0, "cookie": 0, "muffin": 0}

    def buy_food(self, time, file):
        prob_food = {"sandwich": 0, "pie": 0, "cookie": 0, "muffin": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0

        # get percent per hour for food
        for info in file["TIME"]:
            if info == time:
                for key in prob_food:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_food[key] = cum_prob
            where += 1
        # check which kind of food
        for key in prob_food:
            if (prob <= prob_food[key]):
                kind = key
                break
            else:
                kind = "nothing"
        for key in self.history:
            if key == file:
                self.history[key]+=1
        return kind


    def buy_drinks(self, time,file):
        prob_drinks = {"soda": 0, "coffee": 0, "water": 0, "milkshake": 0, "tea": 0, "frappucino": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0

        # get percent per hour for drinks
        for info in file["TIME"]:
            if info == time:
                for key in prob_drinks:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_drinks[key] = cum_prob
            where += 1
        # check which kind of drinks
        for key in prob_drinks:
            if (prob <= prob_drinks[key]):
                kind = key
                break

        for key in self.history:
            if key == kind:
                self.history[key]+=1
        return kind



class returning_customer_regular(returning_customer):

    def __init__(self,*args,**kwargs):
        returning_customer.__init__(self,*args,**kwargs)
        self.budget=250

    def buy_food(self,time,file):
        prob_food = {"sandwich": 0, "pie": 0, "cookie": 0, "muffin": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0

        # get percent per hour for food
        for info in file["TIME"]:
            if info == time:
                for key in prob_food:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_food[key] = cum_prob
            where += 1
        # check which kind of food
        for key in prob_food:
            if (prob <= prob_food[key]):
                kind = key
                break
            else:
                kind = "nothing"


        for key in self.history:
            if key == kind:
                self.history[key]+=1

        return kind


    def buy_drinks(self,time,file):
        prob_drinks = {"soda": 0, "coffee": 0, "water": 0, "milkshake": 0, "tea": 0, "frappucino": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0

        # get percent per hour for drinks
        for info in file["TIME"]:
            if info == time:
                for key in prob_drinks:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_drinks[key] = cum_prob
            where += 1
        # check which kind of drinks
        for key in prob_drinks:
            if (prob <= prob_drinks[key]):
                kind = key
                break

        for key in self.history:
            if key == kind:
                self.history[key]+=1

        return kind

class returning_customer_hipsters(returning_customer):

    def __init__(self,*args,**kwargs):
        returning_customer.__init__(self,*args,**kwargs)
        self.budget=500

    def buy_food(self,time,file):
        prob_food = {"sandwich": 0, "pie": 0, "cookie": 0, "muffin": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0

        # get percent per hour for food
        for info in file["TIME"]:
            if info == time:
                for key in prob_food:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_food[key] = cum_prob
            where += 1
        # check which kind of food
        for key in prob_food:
            if (prob <= prob_food[key]):
                kind = key
                break
            else:
                kind = "nothing"

        for key in self.history:
            if key == kind:
                self.history[key]+=1

        return kind


    def buy_drinks(self,time,file):
        prob_drinks = {"soda": 0, "coffee": 0, "water": 0, "milkshake": 0, "tea": 0, "frappucino": 0}
        prob = random.randint(1, 99)
        where = 0
        cum_prob = 0
        # get percent per hour for drinks
        for info in file["TIME"]:
            if info == time:
                for key in prob_drinks:
                    if file["KIND"][where] == key:
                        cum_prob += file["PERCENTAGE"][where]
                        prob_drinks[key] = cum_prob
            where += 1
        # check which kind of drinks

        for key in prob_drinks:
            if (prob <= prob_drinks[key]):
                kind = key
                break


        for key in self.history:
            if key == kind:
                self.history[key]+=1

        return kind
#tests
"""
test = returning_customer_hipsters("1900")

print(test.history)
print(test.budget)
print(test.buy_drinks(2,12,"8h05","tea"))
print(test.paid)
print(test.history)
print(test.budget)

print(test.buy_drinks(300,29,"8h30","water"))
print(test.paid)
print(test.history)
print(test.budget)

print("---------------")
test2 = once_customer_tripadvisor("3000")
print(test2.budget)
print(test2.buy_drinks(4,12,"8h15","cookie"))
print(test2.paid)
print(test2.budget)
"""

id_client = 1
id = "id_" + str(id_client)

# client = "client_" + id
print(id)

once_customer(2)